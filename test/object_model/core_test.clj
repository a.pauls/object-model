(ns object_model.core_test
  (:require [clojure.test :refer :all]
            [object_model.model :refer :all]))

(defn my-test-fixture [f]
  (def-class :Animal
             (:fields (:name)))
  (def-class :Cat
             (:super :Animal)
             (:fields (:breed)))
  (def-class :Dog
             (:super :Animal)
             (:fields (:weight)))
  (def-class :CatDog
             (:super [:Cat :Dog])
             (:fields (:age)))
  (f)
  )

(use-fixtures :each my-test-fixture)

(deftest class-methods
  (testing "is-class?"
    (is (= true (is-class? :Animal)))
    (is (= false (is-class? 1)))
    (is (= false (is-class? :Not-Animal)))
    (is (= false (is-class? 1))))
  (testing "has-class?"
    (is (= true (has-field? :Animal :name)))
    (is (= nil (has-field? :Animal :name333)))
    (is (= nil (has-field? :Animal :breed)))
    (is (= true (has-field? :Cat :breed))))
  (testing "super-class"
    (is (= [:Cat :Dog] (super-class :CatDog)))
    (is (= [:Object] (super-class :Animal)))
    )
  )

(deftest object-methods
  (let [cat (create-obj :Cat :breed "Siamese" :name "Murka")]
    (testing "getf"
      (is (= "Siamese" (getf cat :breed)))
      (is (= "Murka" (getf cat :name))))
    (testing "setf!"
      (setf! cat :breed "Sphinx")
      (setf! cat :name "Vaska")
      (is (= "Sphinx" (getf cat :breed)))
      (is (= "Vaska" (getf cat :name))))
    )
  )

(deftest function-methods
  (let [cat (create-obj :Cat :breed "Siamese" :name "Murka")
        cat-dog (create-obj :CatDog :breed "Siamese" :name "Kotopes" :weight 10)]
    (def-query speak)
    (def-method speak :Animal [obj]
                (str "Animal say`s: " (getf obj :name)))
    (def-method speak :Cat [obj]
                (str "Cat say`s: " (getf obj :name)))
    ; Method can be redefined
    (def-method speak :Cat [obj]
                (str "Cat say`s: " (getf obj :name)))
    (def-command rename)
    (def-method rename :Animal [obj new-name]
                (setf! obj :name new-name))
    (testing "Use own method"
      (is (= "Cat say`s: Murka" (speak cat))))
    (testing "Use parent method"
      (rename cat "Vaska")
      (is (= "Vaska" (getf cat :name))))
    (testing "Multi-inheritance"
      (def-method speak :Dog [obj] (str "Dog say`s: " (getf obj :name)))
      (is (= "Cat say`s: Kotopes" (speak cat-dog))))
    )
  )

(deftest auxiliary-methods
  (def-method rename :Animal [obj]
              (setf! obj :name (str (getf obj :name) "_animal")))
  (def-method rename :CatDog [obj]
              (setf! obj :name (str (getf obj :name) "_catdog")))
  (testing ":before"
    (let [cat-dog (create-obj :CatDog :breed "Siamese" :name "Kotopes" :weight 10)]
      (def-method rename :Cat :before [obj]
                  (setf! obj :name (str (getf obj :name) "_cat")))
      (rename cat-dog)
      (is (= "Kotopes_cat_catdog" (getf cat-dog :name)))))
  (testing ":after"
    (let [cat-dog (create-obj :CatDog :breed "Siamese" :name "Kotopes" :weight 10)]
      (def-method rename :Cat :after [obj]
                  (setf! obj :name (str (getf obj :name) "_cat")))
      (rename cat-dog)
      (is (= "Kotopes_catdog_cat" (getf cat-dog :name))))
    )
  (testing ":before and :after"
    (let [cat-dog (create-obj :CatDog :breed "Siamese" :name "Kotopes" :weight 10)]
      (def-method rename :Cat :before [obj]
                  (setf! obj :name (str (getf obj :name) "_cat")))
      (def-method rename :Dog :after [obj]
                  ;(call-next-method)
                  (setf! obj :name (str (getf obj :name) "_dog")))
      (rename cat-dog)
      (is (= "Kotopes_cat_catdog_dog" (getf cat-dog :name))))
    )
  (testing "call-next-method"
    (let [cat-dog (create-obj :CatDog :breed "Siamese" :name "Kotopes" :weight 10)]
      (def-method rename :Cat :before [obj]
                  (setf! obj :name (str (getf obj :name) "_cat")))
      (def-method rename :Dog :after [obj]
                  ;(call-next-method)
                  (setf! obj :name (str (getf obj :name) "_dog")))
      (def-method rename :CatDog [obj]
                  (call-next-method)
                  (setf! obj :name (str (getf obj :name) "_catdog")))
      (rename cat-dog)
      (is (= "Kotopes_cat_animal_catdog_dog" (getf cat-dog :name))))
    )
  )
